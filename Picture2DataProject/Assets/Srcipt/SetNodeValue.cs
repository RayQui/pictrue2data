﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class SetNodeValue : FollowNodeUIObject
{
  Node OldNode;
  // Update is called once per frame
  void Update()
  {

    if (FollowingNode == null)
      return;
    else
    {
      _checkNode();
      _follownode();
    }
      
  }
  void _follownode()
  {
    RectTransform.position = FollowingNode.transform.position + followingoffset;
  }
  void _checkNode()
  {
    if (FollowingNode != OldNode)
      ClearText();

      OldNode = FollowingNode;

  }
  void ClearText()
  {
    GetComponent<InputField>().text = "";
  }
  public void SetNodeValueUseUI()
  {
    string value = GetComponent<InputField>().text;
    //Debug.Log("Get Text Form UI : " + value);
    FollowingNode.SetValue(value);
  }
}
