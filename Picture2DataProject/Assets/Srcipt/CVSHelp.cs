﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.IO;

public class CVSHelp  {

  static public void SaveToCSV(DATA[] datas, string FilePath)
  {
    string data = "";
    StreamWriter wr = new StreamWriter(FilePath, false, System.Text.Encoding.Default);
    foreach (DATA d in datas)
    {
      data += d.Level + "," + d.DATA1 + "," + d.DATA2 + "," + d.DATA3 + "\n"; // 換行必須要一個空白
    }
    wr.Write(data);

    wr.Dispose();
    wr.Close();
  }
}
