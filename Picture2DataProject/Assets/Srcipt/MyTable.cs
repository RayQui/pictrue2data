﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MyTable : MappbleDATA
{
  //NodePrefab
  public GameObject NodePrefab;
  //面板物件
  Pad mpad = null;
  //當前點選的Node
  //Node CurrentControllerNode = null;
  //第一個NODE在多高的位置
  public static int TableDefaultLow = -80;
  
  public static int TableDefaultTop = 800;
  //NODE預設深度
  static float NodeDefaultDepth = -8.0f;
  //每個NODE間格多少距離
  static float DistenceEachNode = 10.0f;
  //第一個NODE距離螢幕左邊多少距離
  static float FirstNodeX = -150.0f;
  public int HighLimite = 0;
  public int LowLimite = 0;
  public Node[] Node_List;
  LineRenderer LR;
  Color LRColor;
  public Material LDMat;
  //這個TABLE最後一個NODE的Value
  public float MaxValue;
  //這個TABLE第一個NODE的Value
  public float MinValue;
  // Use this for initialization
  void Start()
  {

  }
  // Update is called once per frame
  void Update()
  {

    if (mpad == null)
    {
      //Debug.Log("mpad == null，請指定");
      mpad = GameObject.Find("Pad").GetComponent<Pad>();
      return;
    }
    //拉線更新必須在MODE移動前
    if (LR != null)
      _updateLDNodePos(Node_List);

  }
 
  /// <summary>
  /// 生出NODE，與LD
  /// </summary>
  /// <param name="level">有多少NODE</param>
  public void CreateNode(int level)
  {
    //取得面板物件
    if (mpad == null)
      mpad = GameObject.Find("Pad").GetComponent<Pad>();

    Node_List = new Node[level];

    //HighLimite = Node_List.Length * mpad.GetLevelY() + (int)TableDefaultLow;
    HighLimite = TableDefaultTop;
    LowLimite = (int)TableDefaultLow;

    for (int i = 0; i < Node_List.Length; i++)
    {
      int nodenum = i + 1;
      GameObject nodego = Instantiate(NodePrefab,transform);
      nodego.name = "node-" + nodenum;
      Node_List[i] = nodego.GetComponent<Node>();
      Node_List[i].SetNode(nodenum,this);
      //Node_List[i].SwitchText(false);
      nodego.transform.position = new Vector3(FirstNodeX + i * DistenceEachNode, mpad.GetLevelY() * i + TableDefaultLow, NodeDefaultDepth);
    }

    Pad.BuildDATAMap(Node_List);

    if (LR == null)
      _createLineRenderer();

  }

  void _createLineRenderer()
  {
    GameObject LDgo = new GameObject("LDgo");
    LDgo.transform.parent = transform;
    LR = LDgo.AddComponent<LineRenderer>();
    //LR.startColor = LRColor;
    //LR.endColor = LRColor;
    LR.material = LDMat;
    LR.material.color = LRColor;
    LR.numCornerVertices = 3;
    LR.SetWidth(0.5f, 0.5f);
  }

  public void SetTable(Color lrcolor,int maxvalue,int minvalue)
  {
    LRColor = lrcolor;
    MaxValue = maxvalue;
    MinValue = minvalue;
  }


  void _updateLDNodePos(Node[] node_list)
  {
    if (node_list.Length < 2)
      return;

    LR.positionCount = node_list.Length;
    int index = 0;
    foreach (Node go in node_list)
    {
      LR.SetPosition(index, go.transform.position);
      index++;
    }

  }

  static public int NodeY2DATA(Node node)
  {
    return (int)node.GetNodeTransY() + (int)TableDefaultLow;
  }

  //static public DATA[] CreateDATA(Node[] node_list1, Node[] node_list2)
  //{
  //  DATA[] datas = new DATA[node_list1.Length];

  //  for (int i = 0; i < node_list1.Length; i++)
  //  {
  //    DATA d = new DATA(i + 1, NodeY2DATA(node_list1[i]), NodeY2DATA(node_list2[i]));
  //  }
  //  return datas;
  //}

  static public int GetNodeDefaultHigh()
  {
    return (int)TableDefaultLow;
  }
  public void DebugNodeList()
  {
    for (int i = 0; i < Node_List.Length; i++)
    {
      Node node = Node_List[i];
      Debug.Log("NodeNum : " + node.GetNodeNum() + "，Value : " + (node.GetNodeTransY() - NodeDefaultDepth) + "TextMValue : " + node.GetTextMValue());
    }
  }

  public MyTable GetMapDate(bool PreOrBack)
  {
    if (PreOrBack)
      return (MyTable)previousdata;
    else
      return (MyTable)behinedata;
  }

  public void SwitchNodeList(bool OC)
  {
    for (int i = 0; i < Node_List.Length; i++)
    {
      Node_List[i].gameObject.SetActive(OC);
    }
    LR.gameObject.SetActive(OC);
  }

  public string ConverHigh2Value(/*float minvalue, float maxvalue, int minposy, int maxposy,*/ float currentposy/*, int offset*/)
  {
    float floor = TableDefaultLow - TableDefaultLow;
    float top = TableDefaultTop - TableDefaultLow;
    float step = currentposy - TableDefaultLow;
    if (step <= 0)
      return MinValue.ToString();
    else if (step >= top)
      return MaxValue.ToString();
    else
    {
      int tmp =  (int)UnityEngine.Mathf.Lerp(MinValue, MaxValue, (step / (top - floor)));
      //float tmp = UnityEngine.Mathf.Lerp(MinValue, MaxValue, (step / (top - floor)));

      return tmp.ToString();
    }
  }

  public float ConverValue2High(string value)
  {
    float floor = MinValue - MinValue;
    float top = MaxValue  - MinValue;
    float step = float.Parse(value) - MinValue;
    //Debug.Log("floor : " + floor);
    //Debug.Log("top : " + top);
    //Debug.Log("step : " + step);

    if (step <= 0)
      return TableDefaultLow;
    else if (step >= top)
      return TableDefaultTop;
    else
    {
      float tmp = UnityEngine.Mathf.Lerp(TableDefaultLow, TableDefaultTop, (step / (top - floor)));
      //Debug.Log("tmp : " + tmp);
      return tmp ;
    }
  }
  //public void SetCurrentNode(Node node)
  //{
  //  CurrentControllerNode = node;
  //}
}
