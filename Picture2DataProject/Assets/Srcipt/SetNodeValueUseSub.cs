﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class SetNodeValueUseSub : FollowNodeUIObject {
  Node OldNode;
  // Update is called once per frame
  void Update()
  {

    if (FollowingNode == null)
      return;
    else
    {
      _checkNode();
      _follownode();
    }

  }
  void _follownode()
  {
    RectTransform.position = FollowingNode.transform.position + followingoffset;
  }
  void _checkNode()
  {
    if (FollowingNode != OldNode)
      ClearText();

    OldNode = FollowingNode;

  }
  void ClearText()
  {
    GetComponent<InputField>().text = "";
  }
  public void SetNodeValueUseS()
  {
    string value = GetComponent<InputField>().text;
    Node preN = (Node)FollowingNode.previousdata;
    int tmp = int.Parse( preN.GetTextMValue());
    //前一個NODE的value + 輸入的值
    tmp += int.Parse(value);
    FollowingNode.SetValue(tmp.ToString());
  }
}
