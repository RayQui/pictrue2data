﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using UnityEngine;
using UnityEngine.UI;


public enum UISwitchMode
{
  SPECIFIC,
  ALL,
  SIZE
}

public class Pad : MonoBehaviour
{

  //LEVEL預設30
  public string Level = "30";
  //每等級預設差距
  string LevelY = "";
  //public string SavePath = "C:\\Users\\User\\Desktop";
  //Table數量
  //public string Table = "3";
  //最高value
  string MaxValue = "1000";
  //最低value
  string MinValue = "200";
  //存檔名稱
  string fileName = "";

  //public Text MaxY = null;
  //操作提示
  public Text Tip = null;

  //string MaxYS = "MaxY : ";

  //table陣列
  List<MyTable> tables = new List<MyTable>();
  //顏色陣列，目前3種
  Color[] Colors = new Color[3] {
    new Color(1.0f,0.5f,0.0f,1.0f),
  Color.blue,
  Color.green};
  //tableprefab
  public GameObject TablePrefab;
  //拍照物件
  SceenShot SS;
  //當前的table
  public MyTable Currenttable = null;
  //滑鼠位置
  Vector2 MousePos;
  //當前點選的Node
  Node CurrentControllerNode = null;
  //可以透過input field 直接對node設定值
  public FollowNodeUIObject[] UI_inputfield = null;



  Camera MainC = null;

  void OnGUI()
  {
    Level = GUI.TextField(new Rect(95, 8, 40, 20), Level, 25);
    LevelY = GUI.TextField(new Rect(115, 31, 40, 20), LevelY, 25);
    //Table = GUI.TextField(new Rect(105, 53, 40, 20), Table, 25);
    MaxValue = GUI.TextField(new Rect(85, 57, 40, 20), MaxValue, 25);
    MinValue = GUI.TextField(new Rect(85, 80, 40, 20), MinValue, 25);
    //SavePath = GUI.TextField(new Rect(20, 280, 400, 20), SavePath, 100);
    fileName = GUI.TextField(new Rect(130, 266, 200, 20), fileName, 100);

    if (GUI.Button(new Rect(20, 150, 100, 50), "Bang"))
    {
      _generalTabel();
    }

    if (GUI.Button(new Rect(20, 210, 100, 50), "Save"))
    {
      //SS.PrintSceen(SavePath);
      save(Application.dataPath);
    }
    //if (GUI.Button(new Rect(130, 210, 100, 50), "Load"))
    //{
    //  //SS.PrintSceen(SavePath);
    //  //load();
    //}
    if (GUI.Button(new Rect(240, 210, 100, 50), "Clear"))
    {
      _clear();
    }

    if (GUI.Button(new Rect(350, 150, 100, 50), "Mix"))
    {
      _showAllTable();
    }

    if (GUI.Button(new Rect(240, 150, 100, 50), ">>"))
    {
      if (tables.Count <= 1)
        return;
      SwitchTable(false);
    }
    if (GUI.Button(new Rect(130, 150, 100, 50), "<<"))
    {
      if (tables.Count <= 1)
        return;
      SwitchTable(true);
    }

    if (GUI.Button(new Rect(130, 210, 100, 50), "ResteCamera"))
    {
      if (MainC == null)
        MainC = Camera.main;

      MainC.transform.position = new Vector3(0.0f, 0.0f, -10.0f);
    }
  }

  // Use this for initialization
  void Start()
  {
    SS = GetComponent<SceenShot>();
    _switchUI(false);
  }

  // Update is called once per frame
  void Update()
  {
    //更新滑鼠位置
    _updatemousepos();

    if (Input.GetMouseButton(1))
    {
      _moveCamera();
    }


    if (Input.GetMouseButtonDown(0))
    {
      _checkClick();
    }

    if (Input.GetMouseButtonUp(0))
    {
      _releaseNode();
      return;
    }

    if (CurrentControllerNode != null)
    {
      //Debug.Log((int)CurrentControllerNode.gameObject.transform.position.y);
      _moveNode();
    }
  }

  /// <summary>
  /// 更新滑鼠位置
  /// </summary>
  void _updatemousepos()
  {
    MousePos = Camera.main.ScreenToWorldPoint(Input.mousePosition);
  }
  /// <summary>
  /// 滑鼠點擊判斷
  /// </summary>
  void _checkClick()
  {
    RaycastHit2D hit = Physics2D.Raycast(Camera.main.ScreenToWorldPoint(Input.mousePosition), Vector2.zero);

    if (hit.collider != null)
    {
      GameObject Nodego = hit.collider.gameObject;
      Node ClickNode = Nodego.GetComponentInParent<Node>();
      CurrentControllerNode = ClickNode;
      ClickNode.SwitchText(true);
      _setUIFollowNode(ClickNode);
      if (ClickNode.GetNodeNum() == 1)
      {
        //只開設定值
        _switchUI(true, UISwitchMode.SPECIFIC, 0);
      }
      else
      {
        //全開
        _switchUI(true);
      }
    }
  }
  /// <summary>
  /// 移動當前NODE
  /// </summary>
  /// <param name="currentcontrollernode">當前NODE</param>
  void _moveNode()
  {
    //Debug.Log("Node : " + currentcontrollernode.GetNodeNum() + "Y數值: " + currentcontrollernode.GetNodeTransY());
    CurrentControllerNode.gameObject.transform.position = new Vector3(CurrentControllerNode.GetNodeTransX(), MousePos.y, CurrentControllerNode.GetNodeTransZ());
  }

  /// <summary>
  /// 滑鼠放開，清除當前NODE
  /// </summary>
  void _releaseNode()
  {
    if (CurrentControllerNode != null)
      CurrentControllerNode = null;
  }


  public int GetLevelY()
  {
    if (string.IsNullOrEmpty(LevelY))
      return MyTable.TableDefaultLow;
    else
      return int.Parse(LevelY);
  }

  //public void SetMaxYText(string value)
  //{
  //  MaxY.text = MaxYS + value;
  //}

  public bool CheckLevelY()
  {
    if (string.IsNullOrEmpty(LevelY) ||
      int.Parse(LevelY) == 0
      )
    {
      return true;
    }
    else
 if (int.Parse(LevelY) > 5 ||
      int.Parse(LevelY) < 0)
    {
      Tip.text = "等級級距不能超過5或是小於0";
      Debug.Log("等級級距不能超過5或是小於0");
      return false;
    }
    return true;
  }

  bool _checkTableCount(bool issave)
  {
    if (!issave)
    {
      if (tables.Count >= 3)
      {
        Tip.text = "Table目前最多三個";
        return false;
      }
    }
    else
    {
      if (tables.Count == 0)
      {
        Tip.text = "Table存檔時不能為0";
        return false;
      }
    }
    return true;
  }
  //bool checkTable()
  //{
  //  int tmp = int.Parse(Table);
  //  if (tmp > 3)
  //  {
  //    Debug.LogError("Table數量目前不能大於3");
  //    return false;
  //  }
  //  else if (tmp < 1)
  //  {
  //    Debug.LogError("Table數量目前不能小於1");
  //    return false;
  //  }
  //  else { return true; }
  //}

  bool checkLevel()
  {
    if (string.IsNullOrEmpty(Level) || int.Parse(Level) < 2/* || int.Parse(Level) > 100*/)
    {
      Tip.text = "LEVEL不能是空白或 < 2，請重新輸入LEVEL";
      Debug.LogError("LEVEL不能是空白或 < 2，請重新輸入LEVEL");
      return false;
    }

    return true;
  }

  bool checkFlieName(string savepsth)
  {
    if (string.IsNullOrEmpty(fileName))
    {
      Tip.text = "FlieName不能是空白";
      Debug.LogError("FlieName 不能是空白");
      return false;
    }
    else
    {
      string filepath = Path.Combine(savepsth, fileName);
      if (File.Exists(filepath))
      {
        Tip.text = "欲儲存檔案名稱 : " + fileName + "已經存在";
        Debug.LogError("LEVEL不能是空白或 < 2或 > 30，請重新輸入LEVEL");
        return false;
      }
      else
      {
        return true;
      }
    }
  }

  //產生table
  void _generalTabel()
  {
    _switchUI(false);
    int leveltmp = int.Parse(Level);

    int maxvalue = int.Parse(MaxValue);
    int minvalue = int.Parse(MinValue);

    //檢查一些設定
    if (!CheckLevelY() ||
      !checkLevel() ||
      !_checkTableCount(false))
    {
      Tip.gameObject.SetActive(true);
      return;
    }
    Tip.gameObject.SetActive(false);

    if (tables == null)
      tables = new List<MyTable>();

    GameObject tablego = Instantiate(TablePrefab);
    // tablego.name = "Table-" + (i + 1);
    MyTable table = tablego.GetComponent<MyTable>();
    table.SetTable(Colors[tables.Count], maxvalue, minvalue);
    tables.Add(table);
    table.CreateNode(leveltmp);

    BuildDATAMap(tables.ToArray());

    //當第二個以上table生成的時候關閉前面只留下當前的
    if (tables.Count > 1)
    {
      for (int i = 0; i < tables.Count - 1; i++)
      {
        tables[i].SwitchNodeList(false);
      }
    }

  }

  void SwitchTable(bool PreOrBack)
  {
    _switchUI(false);
    //關閉目前的
    Currenttable.SwitchNodeList(false);
    Currenttable = Currenttable.GetMapDate(PreOrBack);
    //打開
    Currenttable.SwitchNodeList(true);
  }

  static public void BuildDATAMap(MappbleDATA[] DATAs)
  {
    if (DATAs.Length <= 1)
      return;

    for (int i = 0; i < DATAs.Length; i++)
    {
      try
      {
        if (DATAs[i - 1] != null)
          DATAs[i].previousdata = DATAs[i - 1];
      }
      catch (Exception e)
      {
        DATAs[i].previousdata = DATAs[DATAs.Length - 1];
        DATAs[i].behinedata = DATAs[i + 1];
      }

      try
      {
        if (DATAs[i + 1] != null)
          DATAs[i].behinedata = DATAs[i + 1];
      }
      catch (Exception e)
      {
        DATAs[i].behinedata = DATAs[0];
        DATAs[i].previousdata = DATAs[i - 1];
      }
    }
  }

  void _clear()
  {
    for (int i = 0; i < tables.Count; i++)
    {
      Destroy(tables[i].gameObject);
    }
    tables.Clear();
    _switchUI(false);
    _setUIFollowNode(null);
    fileName = "";
    //Debug.LogError(tables.Count);
  }

  void _showAllTable()
  {
    _switchUI(false);

    for (int i = 0; i < tables.Count; i++)
    {
      tables[i].SwitchNodeList(true);
    }
  }

  public void _moveCamera()
  {
    if (MainC == null)
      MainC = Camera.main;

    float speed = 300.0f;
    MainC.transform.position += new Vector3(-Input.GetAxisRaw("Mouse X") * Time.deltaTime * speed, -Input.GetAxisRaw("Mouse Y") * Time.deltaTime * speed, 0f);
  }

  void _switchUI(bool OnorOff, UISwitchMode mode = UISwitchMode.ALL, int index = 1)
  {
    //全開全關
    if (mode == UISwitchMode.ALL)
    {
      for (int i = 0; i < UI_inputfield.Length; i++)
      {
        UI_inputfield[i].gameObject.SetActive(OnorOff);
      }
    }
    else //只開關特定UI
    if (mode == UISwitchMode.SPECIFIC)
    {
      for (int i = 0; i < UI_inputfield.Length; i++)
      {
        if (i == index)
          UI_inputfield[i].gameObject.SetActive(OnorOff);
      }
    }
  }

  void _setUIFollowNode(Node node)
  {

    for (int i = 0; i < UI_inputfield.Length; i++)
    {
      UI_inputfield[i].FollowingNode = node;
    }

  }

  public void save(string savepath)
  {
    if (!checkFlieName(savepath) || !_checkTableCount(true))
    {
      Tip.gameObject.SetActive(true);
      return;
    }

    Tip.gameObject.SetActive(false);

    //暫時以第一個tabel的NODE數量當作資料的長度
    DATA[] datas = new DATA[tables[0].Node_List.Length];


    for (int i = 0; i < tables.Count; i++)
    {
      for (int j = 0; j < tables[i].Node_List.Length; j++)
      {
        if (i == 0)
        {
          if (datas[j] == null)
            datas[j] = new DATA();
          datas[j].Level = (j + 1);
          datas[j].DATA1 = tables[i].Node_List[j].GetTextMValue();
        }
        if (i == 1)
          datas[j].DATA2 = tables[i].Node_List[j].GetTextMValue();
        if (i == 2)
          datas[j].DATA3 = tables[i].Node_List[j].GetTextMValue();
      }
    }
    String destinationPath = Path.Combine(savepath, fileName);

    ////Json沒辦法直接對array儲存
    //string saveString = JsonHelp.arrayToJson< DATA >(datas);

    ////將字串saveString存到硬碟中
    //StreamWriter file = new StreamWriter(destinationPath);
    //file.Write(saveString);
    //file.Close();

    CVSHelp.SaveToCSV(datas, destinationPath + ".csv");

    Tip.text = "成功存檔，path : " + destinationPath;
    Tip.gameObject.SetActive(true);
  }

}
