﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Node : MappbleDATA
{

  int NodeNum;
  //public int HighLimite = 0;
  //public int LowLimite = 0;
  public TextMesh TextM;
  public TextMesh NumTextM;
  //顯示與上一個NODE value的差值
  public TextMesh Textsub;
  public bool canmove;
  public MyTable mytable;
  //public MappbleDATA PreviousNode;
  //public MappbleDATA behindNode;
  // Use this for initialization
  void Start()
  {

  }

  // Update is called once per frame
  void Update()
  {
    _HighCheck();
    _updateTextM();
    _updateNum();
    _updatesub();
  }

  public float GetNodeTransZ()
  {
    return transform.position.z;
  }

  public float GetNodeTransY()
  {
    return transform.position.y;
  }

  public int GetNodeTransX()
  {
    return (int)transform.position.x;
  }

  public int GetNodeNum()
  {
    return NodeNum;
  }

  public string GetTextMValue()
  {
    return TextM.text;
  }

  void _HighCheck()
  {
    if (previousdata != null && NodeNum != 1)
    {
      Node tmp = (Node)previousdata;
      if (transform.position.y < tmp.GetNodeTransY())
      {
        transform.position = new Vector3(transform.position.x, tmp.GetNodeTransY(), transform.position.z);
        return;
      }
    }
    if ((int)transform.position.y > mytable.HighLimite)
    {
      transform.position = new Vector3(transform.position.x, mytable.HighLimite, transform.position.z);
      canmove = false;
      return;
    }
    if ((int)transform.position.y < mytable.LowLimite)
    {
      transform.position = new Vector3(transform.position.x, mytable.LowLimite, transform.position.z);
      canmove = false;
      return;
    }
    canmove = true;

  }

  //public void SetLimite(int limiteh, int limitel)
  //{
  //  HighLimite = limiteh;
  //  LowLimite = limitel;
  //}
  public void SetNode(int num,MyTable mytable)
  {
    NodeNum = num;
    this.mytable = mytable;
    //SetLimite(limiteh, limite);
  }
  public void SwitchText(bool OnOff)
  {
    TextM.gameObject.SetActive(OnOff);
  }
  public void _updateTextM()
  {
    //int tmp = (int)transform.position.y - LowLimite;
    ////Debug.Log("NodeNum : " + NodeNum + "，node Ty: " + (int)transform.position.y + "，LowLimite " + LowLimite + "，Total:" + tmp);
    //TextM.text = tmp.ToString();
    string value = mytable.ConverHigh2Value(/*mytable.MinValue, mytable.MaxValue, MyTable.TableDefaultLow, MyTable.TableDefaultTop, */transform.position.y/*, MyTable.TableDefaultLow*/);
    TextM.text = value;
  }
  public void _updateNum()
  {
    NumTextM.text = NodeNum.ToString();
  }

  public void SetValue(string value)
  {
    //Debug.Log("Value Form Input Field : " + value);
    float conversionhigh = mytable.ConverValue2High(value);
   // Debug.Log("Conversion High Form Input Field Value : " + conversionhigh);

    float conversionvalue = float.Parse( mytable.ConverHigh2Value(conversionhigh));

    //Debug.Log("Conversion Value Form conversionvalue : " + conversionvalue);

    //如果輸入的的值 != 轉換的conversionvalue，目前只有出現過筆輸入的小沒有比輸入的大
    //那我就重算一次，+上value上升0.1的高度值
    if (!value.Equals(conversionvalue))
      conversionhigh += (0.001f * (MyTable.TableDefaultTop - MyTable.TableDefaultLow)) * (100.0f / (mytable.MaxValue - mytable.MinValue)) ;

    //Debug.Log("2Conversion High Form Input Field Value : " + conversionhigh);


    conversionvalue = float.Parse(mytable.ConverHigh2Value(conversionhigh));

    Debug.Log("2Conversion Value Form conversionvalue : " + conversionvalue);

    transform.position = new Vector3(transform.position.x,conversionhigh, transform.position.z);
  }

  void _valueup(float wantupvalue)
  {
    //t 1000 上升 1  >10
    //t 5900 生商1 > 59

    //先算出上升value1是佔總value多少百分比
    //float tmp = wantupvalue / (mytable.MaxValue - mytable.MaxValue);
    //float needhgih = tmp * ( MyTable.TableDefaultTop - MyTable.TableDefaultLow * 0.01f);
  }


  //更新差值
  public void _updatesub()
  {
    //第一個Node是沒有差值得
    if (NodeNum == 1)
    {
      Textsub.text = "";
      return;
    }
      
    if (previousdata == null)
      return;

    Node preN = (Node)previousdata;
    int preNvalue = int.Parse (preN.GetTextMValue());
    int subvalue =  int.Parse(GetTextMValue()) - preNvalue;

    Textsub.text = subvalue.ToString();

  }
}
